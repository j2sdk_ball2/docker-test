docker stop app
docker rm app
docker rmi app
docker build -t app app/.
docker run --name=app -v $(pwd)/app/src/:/usr/src/app/src -d app